function socket(io) {
  // start listen with socket.io
  io.on('connection', function(socket){

    console.log('user connected');

    io.emit('server online');

    socket.on('client online', function(data){
      // test = data.username;

      var data = {
        system: true,
        username: data.username,
        message: data.message,
        date: Date.now()
      };

      console.log('online', JSON.stringify(data, null, '  '));
      // console.log(test);

      socket.broadcast.emit('message here', data);

    });

    socket.on('message', function(data){

      var data = {
        username: data.username,
        avatar: data.avatar,
        message: data.message,
        date: Date.now()
      };

      console.log('message', JSON.stringify(data, null, '  '));

      if(data.room != null) {
        io.to(data.room).emit('message', data);
      } else {
        io.emit('message', data);
      }

    });

    socket.on('join', function(room){
      socket.join(room);
    });

    socket.on('leave', function(room){
      socket.leave(room);
    });

    socket.on('disconnect', function(){

      console.log('dissconnect');

      io.emit('server offline');

      socket.on('client offline', function(data){

        var data = {
          system: true,
          username: data.username,
          message: data.message,
          date: Date.now()
        };

        console.log('offline', JSON.stringify(data, null, '  '));

        socket.emit('message', data);

      });

    });

  });
}

module.exports = socket;
