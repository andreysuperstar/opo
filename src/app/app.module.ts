import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Storage } from '@ionic/storage';

import {
  WelcomePage,
  LoginPage,
  SignupPage,
  TermsPage,
  PasswordPage,
  MutualFriendsPage,
  ProfileSetupPage,
  ProfilePage,
  ProfilePopoverPage,
  ProfileEditPage,
  ProfileMainPage,
  ProfileScorePage,
  ProfileScoreboardPage,
  ProfileLookbookPage,
  ProfileLookbookItemPage,
  ProfileLookbookItemOverviewPage,
  ProfileLookbookItemPopoverPage,
  ProfileLookbookItemEditPage,
  ProfileOtherPage,
  RoomPage,
  AddProductPage,
  ProductPage,
  FriendsPage,
  FriendsTabPage,
  RequestsTabPage,
  FollowingTabPage,
  FollowersTabPage,
  GroupsTabPage,
  ClientFeedbackPage,
  FaqPage,
  AboutPage,
  AppGeneralSettingsPage,
  SearchPage,
  HomeFeedPage,
  AllTabPage,
  MyOpoTabPage,
  OpoVotesTabPage,
  CreateRoomPage,
  GroupPage,
  GroupPopoverPage,
  AddRemoteFriendsPage,
  FindFriendsPage,
  NetworkFriendsPage,
  AddFriendsPage,
  InviteFriendsPage,
  InviteFriendsTabPage,
  ShareTabPage,
  CreateItemPage,
  AddItemPage,
  MembersListPage
} from '../pages';

import { MenuComponent, ProfileCardComponent, VotesCardComponent, LookbookCardComponent } from '../components';

import { DataService } from '../providers/data';


@NgModule({
  declarations: [
    ProfileCardComponent,
    VotesCardComponent,
    LookbookCardComponent,
    MyApp,
    WelcomePage,
    LoginPage,
    SignupPage,
    TermsPage,
    PasswordPage,
    MutualFriendsPage,
    ProfileSetupPage,
    ProfilePage,
    ProfilePopoverPage,
    ProfileEditPage,
    ProfileMainPage,
    ProfileScorePage,
    ProfileScoreboardPage,
    ProfileLookbookPage,
    ProfileLookbookItemPage,
    ProfileLookbookItemPopoverPage,
    ProfileLookbookItemOverviewPage,
    ProfileLookbookItemEditPage,
    ProfileOtherPage,
    HomeFeedPage,
    RoomPage,
    CreateRoomPage,
    AddProductPage,
    ProductPage,
    FriendsPage,
    FriendsTabPage,
    RequestsTabPage,
    FollowingTabPage,
    FollowersTabPage,
    GroupsTabPage,
    SearchPage,
    AllTabPage,
    MyOpoTabPage,
    OpoVotesTabPage,
    ClientFeedbackPage,
    FaqPage,
    AboutPage,
    AppGeneralSettingsPage,
    GroupPage,
    GroupPopoverPage,
    AddRemoteFriendsPage,
    FindFriendsPage,
    NetworkFriendsPage,
    AddFriendsPage,
    InviteFriendsPage,
    InviteFriendsTabPage,
    ShareTabPage,
    CreateItemPage,
    AddItemPage,
    MembersListPage,
    MenuComponent
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      backButtonText: ''
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WelcomePage,
    LoginPage,
    SignupPage,
    TermsPage,
    PasswordPage,
    MutualFriendsPage,
    ProfileSetupPage,
    ProfilePage,
    ProfilePopoverPage,
    ProfileEditPage,
    ProfileMainPage,
    ProfileScorePage,
    ProfileScoreboardPage,
    ProfileLookbookPage,
    ProfileLookbookItemPage,
    ProfileLookbookItemPopoverPage,
    ProfileLookbookItemOverviewPage,
    ProfileLookbookItemEditPage,
    ProfileOtherPage,
    HomeFeedPage,
    RoomPage,
    CreateRoomPage,
    AddProductPage,
    ProductPage,
    FriendsPage,
    FriendsTabPage,
    RequestsTabPage,
    FollowingTabPage,
    FollowersTabPage,
    GroupsTabPage,
    SearchPage,
    AllTabPage,
    MyOpoTabPage,
    OpoVotesTabPage,
    ClientFeedbackPage,
    FaqPage,
    AboutPage,
    AppGeneralSettingsPage,
    GroupPage,
    GroupPopoverPage,
    AddRemoteFriendsPage,
    FindFriendsPage,
    NetworkFriendsPage,
    AddFriendsPage,
    InviteFriendsPage,
    InviteFriendsTabPage,
    ShareTabPage,
    CreateItemPage,
    AddItemPage,
    MembersListPage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    DataService,
    Storage
  ]
})
export class AppModule { }




