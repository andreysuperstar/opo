import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { WelcomePage } from '../pages/global/welcome/welcome';
import { AboutPage } from '../pages/global/about/about';
import { RoomPage } from '../pages/opo-room/room/room';
import { HomeFeedPage } from '../pages/home-feed/home-feed';

declare var Branch;

@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  @ViewChild(Nav) nav: Nav;
  rootPage: any = WelcomePage;
  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform) {
    this.pages = [
      { title: 'About', component: AboutPage },
      { title: 'Room', component: RoomPage },
      { title: 'My Feed', component: HomeFeedPage }
    ];
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();

      this.branchInit();

      // Triggers hardware Back button
      // this.platform.registerBackButtonAction(() => {
      //   console.log('back button pressed');
      //   this.menuCtrl.open();
      // }, 100);
    });

    this.platform.resume
      .subscribe(
        () => this.branchInit()
      );
  }

  branchInit() {
    Branch.initSession((data) => {})
      .then(
        (data) => {
          if (data.screen || data.screen && data.id)
            this.branchNavigate(data);
        }
      )
      .catch(
        (error) => alert(`Error: ${JSON.stringify(error)}`)
      );
  }

  branchNavigate(data) {
    this.openPage(data);
  }

  openPage(data) {
    let page = this.pages.find(page => page.title == data.screen);
    if (data.id)
      this.nav.setRoot(page.component, { id: data.id });
    else
      this.nav.setRoot(page.component);
  }

}
