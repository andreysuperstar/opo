import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DataService } from '../../../../providers/data';

@Component({
  selector: 'page-opo-votes-tab',
  templateUrl: 'opo-votes-tab.html'
})
export class OpoVotesTabPage {

  friends: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataService) {}

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.friends = data.friends;
    });
  }

}


