import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DataService } from '../../../../providers/data';

@Component({
  selector: 'page-my-opo-tab',
  templateUrl: 'my-opo-tab.html'
})
export class MyOpoTabPage {

  friends: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataService) { }

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.friends = data.friends;
    });
  }


}
