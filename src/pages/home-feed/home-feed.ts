import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AllTabPage } from './tabs/all-tab/all-tab';
import { MyOpoTabPage } from './tabs/my-opo-tab/my-opo-tab';
import { OpoVotesTabPage } from './tabs/opo-votes-tab/opo-votes-tab';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home-feed',
  templateUrl: 'home-feed.html'
})
export class HomeFeedPage {

  tabAll = AllTabPage;
  tabMyOpo = MyOpoTabPage;
  tabOpoVotes = OpoVotesTabPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) { }

  ionViewDidLoad(){
      // Or to get a key/value pair
      this.storage.get('user').then((user) => {
        console.log(user);
      })
  }


}
