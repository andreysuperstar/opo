import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-network-friends',
  templateUrl: 'network-friends.html'
})
export class NetworkFriendsPage {

  friends: any;
  title: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    this.friends = this.navParams.get('friends');
    this.title = this.navParams.get('title');
  }

  inviteCurrentFriend(friend) {
    console.log(friend);
  }

  onDone() {
    console.log('Done');
  }

}
