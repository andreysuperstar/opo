import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

import { ProfileMainPage } from './tabs/profile-main/profile-main';
import { ProfileLookbookPage } from './tabs/profile-lookbook/profile-lookbook';
import { ProfileScorePage } from './tabs/profile-score/profile-score';
// import { FriendsPage } from '../friends/friends';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  main: any = ProfileMainPage;
  score: any = ProfileScorePage;
  lookbook: any = ProfileLookbookPage;
  // friends: any = FriendsPage;

  // user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, public menu: MenuController) {
    // this.user = this.navParams.get('user');
  }

  ionViewDidLoad() {
    this.menu.enable(true);
    this.menu.swipeEnable(true);
  }

  // ionViewWillEnter() {
  //   // this.menuCtrl.open();
  // }

}
