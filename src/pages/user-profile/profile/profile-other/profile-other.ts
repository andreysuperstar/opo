import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { ProfilePopoverPage } from '../tabs/profile-popover/profile-popover';
import { RoomPage } from '../../../opo-room/room/room';
import { DataService } from '../../../../providers/data';

@Component({
  selector: 'page-profile-other',
  templateUrl: 'profile-other.html'
})
export class ProfileOtherPage {

  user: { name?: string, picture?: string } = {};
  newUser: { id?: number, name?: string, email?: string, password?: string, picture?: string, avatar?: string } = {};
  profilePicture: string;
  profileBackground: any;
  friends: any[];
  friend: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public data: DataService) {
    this.user.name = 'Michael Indursky';
    this.friend = { name: 'Steve Ayzen' };
    this.profilePicture = 'assets/img/michael-indursky.png';
    this.profileBackground = {
      'background-image': `url(${this.profilePicture})`
    };
  }

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.friends = data.friends;
    });
    console.log('user', this.navParams.data);
    this.newUser = this.navParams.data;
    console.log(this.newUser);
    if (this.newUser.name) {
      this.user = this.newUser;
      this.profileBackground = {
        'background-image': `url(${this.newUser.picture})`
      };
    }

    this.user.name = this.navParams.get('user').name;
    this.profilePicture = this.navParams.get('user').picture;
    this.profileBackground = {
      'background-image': `url(${this.profilePicture})`
    };
    console.log('Profile Other', this.profileBackground);
  }

  presentPopover() {
    let options = {
      cssClass: 'profile-popover',
      showBackdrop: true
    };
    let popover = this.popoverCtrl.create(ProfilePopoverPage, { user: this.user }, options);
    console.log('go to popover', this.user);
    popover.present();
  }

  navigateToRoom() {
    this.navCtrl.push(RoomPage, { user: this.user });
  }

}
