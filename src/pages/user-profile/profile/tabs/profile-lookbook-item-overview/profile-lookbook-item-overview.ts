import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-profile-lookbook-item-overview',
  templateUrl: 'profile-lookbook-item-overview.html'
})
export class ProfileLookbookItemOverviewPage {
  photos: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public alertCtrl: AlertController) {
    this.photos = this.navParams.get('photos');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  trash() {
    let confirm = this.alertCtrl.create({
      title: 'Remove All Hash tag from selected contacts',
      // message: 'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

}

