import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-profile-lookbook-item-edit',
  templateUrl: 'profile-lookbook-item-edit.html'
})
export class ProfileLookbookItemEditPage {

  photo: string;
  name: string;
  datetime: string;
  participants: number;
  score: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.photo = 'assets/img/fashion-6.jpg';
    this.name = 'Shoes';
    this.datetime = new Date('2016-12-11T11:20:00').toISOString();
    this.participants = 40;
    this.score = 20;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileLookbookItemEditPage');
  }

  cancel() {
    this.navCtrl.pop();
  }

}
