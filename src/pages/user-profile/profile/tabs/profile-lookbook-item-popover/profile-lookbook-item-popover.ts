import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/*
  Generated class for the ProfileLookbookItemOverviewPopover page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile-lookbook-item-popover',
  templateUrl: 'profile-lookbook-item-popover.html'
})
export class ProfileLookbookItemPopoverPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileLookbookItemPopoverPage');
  }

  delete () {
    // this.navCtrl.push();
    this.close();
  }

  share () {
    // this.navCtrl.push();
    this.close();
  }

  details () {
    // this.navCtrl.push();
    this.close();
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
