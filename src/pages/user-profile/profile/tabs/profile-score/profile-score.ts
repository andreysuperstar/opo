import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { ProfilePopoverPage } from '../profile-popover/profile-popover';
import { ProfileScoreboardPage } from '../profile-scoreboard/profile-scoreboard';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-profile-score',
  templateUrl: 'profile-score.html'
})
export class ProfileScorePage {

  profileBackground: any;
  friends: any[];
  images: any[];
  user: { name?: string, picture?: string } = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public storage: Storage) {
    // this.profileBackground = {
    //   'background-image': `url(${this.profilePicture})`
    // };

    this.friends = [
      { name: 'Susan Lee', avatar: 'assets/img/susan.jpg' },
      { name: 'Lori Gomez', avatar: 'assets/img/lori.jpg' },
      { name: 'Heather Porter', avatar: 'assets/img/heather.jpg' },
      { name: 'Sarah Mendoza', avatar: 'assets/img/sarah.jpg' },
      { name: 'Joan Bishop', avatar: 'assets/img/joanb.jpg' },
      { name: 'Melissa Guzman', avatar: 'assets/img/mellisa2.jpg' },
      { name: 'Randy Sanders', avatar: 'assets/img/randy4.jpg' },
      { name: 'Pete Guerin', avatar: 'assets/img/pete2.jpg' },
      { name: 'Andrew Ladd', avatar: 'assets/img/andrew.jpg' },
      { name: 'Tomas Beleskey', avatar: 'assets/img/thomas2.jpg' },
      { name: 'Helen Scott', avatar: 'assets/img/helen.jpg' },
      { name: 'Kyle Norman', avatar: 'assets/img/kyle.jpg' },
      { name: 'Kate Ruth', avatar: 'assets/img/kate.jpg' },
      { name: 'Jeremy Bradley', avatar: 'assets/img/jeremy.jpg' }
    ];

    this.images = [
      [
        // { src: 'assets/img/fashion1.jpeg' },
        { src: 'assets/img/fashion5.jpeg' },
        { src: 'assets/img/fashion7.jpeg' }
      ],
      [
        // { src: 'assets/img/fashion10.jpeg' },
        { src: 'assets/img/fashion8.jpeg' },
        { src: 'assets/img/fashion6.jpeg' }
      ],
      [
        // { src: 'assets/img/fashion9.jpeg' },
        { src: 'assets/img/fashion1.jpeg' },
        { src: 'assets/img/fashion10.jpeg' }
      ],
      [
        { src: 'assets/img/fashion2.jpeg' },
        { src: 'assets/img/fashion7.jpeg' }
      ]
    ];
  }

  ionViewDidLoad() {
      this.storage.get('user').then((data) => {
        this.user = data;
        this.profileBackground = {
          'background-image': `url(${data.picture})`
        };
    });
  }

  presentPopover() {
    let options = {
      cssClass: 'profile-popover',
      showBackdrop: true
    };
    let popover = this.popoverCtrl.create(ProfilePopoverPage, {}, options);
    popover.present();
  }

  navigateScoreboard() {
    this.navCtrl.push(ProfileScoreboardPage);
  }

}
