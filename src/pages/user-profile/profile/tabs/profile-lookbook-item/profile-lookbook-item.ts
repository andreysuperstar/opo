import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, PopoverController } from 'ionic-angular';
import { ProfileLookbookItemPopoverPage } from '../profile-lookbook-item-popover/profile-lookbook-item-popover';
import { ProfileLookbookItemOverviewPage } from '../profile-lookbook-item-overview/profile-lookbook-item-overview';

@Component({
  selector: 'page-profile-lookbook-item',
  templateUrl: 'profile-lookbook-item.html'
})
export class ProfileLookbookItemPage {

  images: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public popoverCtrl: PopoverController) {
    this.images = [
      {
        category: 'Shoes',
        date: Date.now(),
        photos: [
          { src: 'assets/img/fashion1.jpeg' },
          { src: 'assets/img/fashion5.jpeg' },
          { src: 'assets/img/fashion7.jpeg' }
        ]
      },
      {
        category: 'T-Shirt',
        date: Date.now(),
        photos: [
          { src: 'assets/img/fashion10.jpeg' },
          { src: 'assets/img/fashion8.jpeg' },
          { src: 'assets/img/fashion6.jpeg' }
        ]
      },
      {
        category: 'Dress',
        date: Date.now(),
        photos: [
          { src: 'assets/img/fashion9.jpeg' },
          { src: 'assets/img/fashion1.jpeg' },
          { src: 'assets/img/fashion10.jpeg' }
        ]
      }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileLookbookItemPage');
  }

  openPhoto(photos) {
    let modal = this.modalCtrl.create(ProfileLookbookItemOverviewPage, {
      photos: photos
    });
    modal.present();
  }

  presentPopover() {
    let options = {
      cssClass: 'profile-lookbook-item-overview-popover',
      showBackdrop: true
    };
    let popover = this.popoverCtrl.create(ProfileLookbookItemPopoverPage, {}, options);
    popover.present();
  }

}
