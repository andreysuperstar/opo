import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { ProfilePopoverPage } from '../profile-popover/profile-popover';
import { ProfileLookbookItemPage } from '../profile-lookbook-item/profile-lookbook-item';
import { DataService } from '../../../../../providers/data';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-profile-lookbook',
  templateUrl: 'profile-lookbook.html'
})
export class ProfileLookbookPage {

  profilePicture: string;
  profileBackground: any;
  friends: any[] = [];
  images: any[] = [];
  user: { name?: string, picture?: string } = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public data: DataService, public storage: Storage) {
    this.profilePicture = 'assets/img/profile-picture.jpg';
  };

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.friends = data.friends;
    });

    this.storage.get('user').then((data) => {
      this.user = data;
      this.profileBackground = {
        'background-image': `url(${data.picture})`
      };
    });
  }



  presentPopover() {
    let options = {
      cssClass: 'profile-popover',
      showBackdrop: true
    };
    let popover = this.popoverCtrl.create(ProfilePopoverPage, {}, options);
    popover.present();
  }

  openItem(item) {
    this.navCtrl.push(ProfileLookbookItemPage);
  }

}
