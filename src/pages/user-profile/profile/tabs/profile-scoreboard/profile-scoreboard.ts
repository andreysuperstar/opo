import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { ProfilePopoverPage } from '../profile-popover/profile-popover';

@Component({
  selector: 'page-profile-scoreboard',
  templateUrl: 'profile-scoreboard.html'
})
export class ProfileScoreboardPage {

  profilePicture: string;
  profileBackground: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController) {
    this.profilePicture = 'assets/img/profile-picture.jpg';
    this.profileBackground = {
      'background-image': `url(${this.profilePicture})`
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileScoreboardPage');
  }

  presentPopover() {
    let options = {
      cssClass: 'profile-popover',
      showBackdrop: true,
    };
    let popover = this.popoverCtrl.create(ProfilePopoverPage, {}, options);
    popover.present();
  }

}
