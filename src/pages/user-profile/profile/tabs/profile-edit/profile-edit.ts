import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ActionSheetController, AlertController } from 'ionic-angular';
import { Camera } from 'ionic-native';

@Component({
  selector: 'page-profile-edit',
  templateUrl: 'profile-edit.html'
})
export class ProfileEditPage {

  user: { id?: number, name?: string, email?: string, password?: string, picture?: string, avatar?: string } = {};
  avatar: string;
  name: string;
  email: string;
  gender: string;
  date: string;
  password: string;
  tel: string;
  bio: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public actionSheetCtrl: ActionSheetController, public alertCtrl: AlertController) {
    this.user.name = 'Olive Perez';
    this.user.email = 'leuschke_tobin@sheila.us';
    this.date = new Date('2016-12-11T11:20:00').toISOString();
    this.password = '123456';
    this.tel = '777-692-4071';
  }

  ionViewDidLoad() {
    this.viewCtrl.showBackButton(false);
    this.user = this.navParams.get('user');
    console.log(this.user);
  }

  cancel() {
    this.navCtrl.pop();
  }

  save() {
    console.log('save');
    this.navCtrl.pop();
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Choose the action',
      buttons: [
        {
          text: 'Gallery',
          handler: () => {
            this.openGallery();
          }
        }, {
          text: 'Take Photo',
          role: 'destructive',
          handler: () => {
            this.takePhoto();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  openGallery() {
    let options = {
      quality: 100,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: Camera.EncodingType.JPEG,
      correctOrientation: true,
      allowEdit: true,
      targetWidth: 800,
      targetHeight: 800
    };
    Camera.getPicture(options).then((imageData) => {
      this.avatar = "data:image/jpeg;base64," + imageData;
    }, (err) => {
      // Handle error
    });
  }

  takePhoto() {
    let options = {
      quality: 100,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 800,
      targetHeight: 800,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };;
    Camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.avatar = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });

  }

  updatePasswordAlert() {
    let alert = this.alertCtrl.create({
      title: 'Update password',
      inputs: [
        {
          name: 'old password',
          placeholder: 'old password'
        },
        {
          name: 'new password',
          placeholder: 'new password',
          type: 'password'
        },
        {
          name: 're-password',
          placeholder: 're-password',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Update password',
          handler: data => {
            console.log(data);
          }
        }
      ]
    });
    alert.present();
  }

}
