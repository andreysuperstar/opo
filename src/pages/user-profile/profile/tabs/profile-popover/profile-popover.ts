import { Component } from '@angular/core';
import { App, NavController, NavParams, ViewController } from 'ionic-angular';
import { ProfileEditPage } from '../profile-edit/profile-edit';
import { ClientFeedbackPage } from '../../../../../pages/global/client-feedback/client-feedback';
import { FaqPage } from '../../../../../pages/global/faq/faq';

@Component({
  selector: 'page-profile-popover',
  templateUrl: 'profile-popover.html'
})
export class ProfilePopoverPage {

  user: any;

  constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePopoverPage');
    this.user = this.navParams.get('user');
  }

  editProfileInfo() {
    this.app.getRootNav().push(ProfileEditPage, { user: this.user });
    console.log('go to edit-profile', this.user);
    this.close();
  }

  toFAQ() {
    this.app.getRootNav().push(FaqPage, { user: this.user });
    console.log('go to edit-profile', this.user);
    this.close();
  }
  toFeedback() {
    this.app.getRootNav().push(ClientFeedbackPage, { user: this.user });
    this.close();
  }


  editProfilePicture() {
    this.app.getRootNav().push(ProfileEditPage, { user: this.user });
    this.close();
  }

  editProfileSettings() {
    this.app.getRootNav().push(ProfileEditPage, { user: this.user });
    this.close();
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
