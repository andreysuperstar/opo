import { Component } from '@angular/core';
import { App, NavController, NavParams, PopoverController } from 'ionic-angular';
import { DataService } from '../../../../../providers/data';
import { ProfilePopoverPage } from '../profile-popover/profile-popover';
import { RoomPage } from '../../../../opo-room/room/room';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-profile-main',
  templateUrl: 'profile-main.html'
})
export class ProfileMainPage {

  user: { name?: string, picture?: string } = {};
  newUser: { id?: number, name?: string, email?: string, password?: string, picture?: string, avatar?: string } = {};
  profilePicture: string;
  profileBackground: any;
  friends: any[];
  friend: any;

  constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public data: DataService, public storage: Storage) {
    this.friend = { name: 'Hettie Grey' };
  }

  ionViewDidEnter() {
    this.data.getData().subscribe(data => {
      this.friends = data.friends;
    });

    this.storage.get('user').then((data) => {
        if (data) {
          this.user = data;
          console.log('Main Profile', data);
          this.profileBackground = {
            'background-image': `url(${data.picture})`
          };
        }
    });
  }

  presentPopover() {
    let options = {
      cssClass: 'profile-popover',
      showBackdrop: true
    };
    let popover = this.popoverCtrl.create(ProfilePopoverPage, { user: this.user }, options);
    console.log('go to popover', this.user);
    popover.present();
  }

  navigateToRoom() {
    this.app.getRootNav().push(RoomPage, { user: this.user });
  }

}