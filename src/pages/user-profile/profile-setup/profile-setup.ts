import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, AlertController } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { MutualFriendsPage } from '../../global/mutual-friends/mutual-friends';


@Component({
  selector: 'page-profile-setup',
  templateUrl: 'profile-setup.html'
})
export class ProfileSetupPage {

  myDate: Object = new Date().toISOString();
  myStatus: String = "Ordering Contact Lens Online";
  public base64Image: string;

  photoTaken: boolean;
  cameraUrl: string;
  photoSelected: boolean;


  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, private alertCtrl: AlertController) { }


  takePhoto() {
    let options = {
      quality: 100,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 800,
      targetHeight: 800,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    Camera.getPicture(options).then((imageData) => {
      this.base64Image = "data:image/jpeg;base64," + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  selectFromGallery() {
    let options = {
      quality: 100,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 800,
      targetHeight: 800,
      correctOrientation: true
    };
    Camera.getPicture(options).then((imageData) => {
      this.base64Image = "data:image/jpeg;base64," + imageData;
    }, (err) => {
      // Handle error
    });

  }

  uploadImgActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Choose the action',
      buttons: [
        {
          text: 'Gallery',
          handler: () => {
            this.selectFromGallery();
          }
        }, {
          text: 'Take photo',
          role: 'destructive',
          handler: () => {
            this.takePhoto();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  updatePasswordAlert() {
    let alert = this.alertCtrl.create({
      title: 'Update password',
      inputs: [
        {
          name: 'old password',
          placeholder: 'old password'
        },
        {
          name: 'new password',
          placeholder: 'new password',
          type: 'password'
        },
        {
          name: 're-password',
          placeholder: 're-password',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Update password',
          handler: data => {
            console.log(data);
          }
        }
      ]
    });
    alert.present();
  }

  clickTwitter() {
    console.log('Twitter');
  }

  clickFacebook() {
    console.log('Facebook');
  }

  clickYoutube() {
    console.log('YouTube');
  }

  clickSave() {
    console.log('Save');
    this.navCtrl.setRoot(MutualFriendsPage);
  }
}

