import { Component } from '@angular/core';
import { App, NavController, NavParams, PopoverController } from 'ionic-angular';
import { DataService } from '../../../providers/data';
import { GroupPopoverPage } from '../group-popover/group-popover';
import { ProfilePage } from '../profile/profile';
// import { FriendsPage } from '../friends/friends';

@Component({
  selector: 'page-group',
  templateUrl: 'group.html'
})
export class GroupPage {

  // countRequests: number;
  group: any;
  title: string;
  friends: any;
  myInput: string;

  constructor(public app: App, public navCtrl: NavController, public data: DataService, public params: NavParams, public popoverCtrl: PopoverController) {

  }

  ionViewDidLoad() {

    this.group = this.params.get('group');
    this.friends = this.group.friends;
    this.title = this.group.name;

    this.data.getData().subscribe(data => {
      // this.countRequests = data.requests.length;
    });
    this.myInput = null;
  }

  presentPopover() {
    let options = {
      cssClass: 'group-popover',
      showBackdrop: true
    };
    let popover = this.popoverCtrl.create(GroupPopoverPage, { title: this.title, friends: this.friends }, options);
    popover.present();
  }

  navigateToProfile() {
    this.app.getRootNav().push(ProfilePage);
  }

  // cancel() {
    // this.app.getRootNav().setRoot(FriendsPage);
    // this.navCtrl.pop();
  // }

}
