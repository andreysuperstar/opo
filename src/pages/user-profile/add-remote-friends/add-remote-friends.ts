import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-add-remote-friends',
  templateUrl: 'add-remote-friends.html'
})
export class AddRemoteFriendsPage {

  title: string;
  friends: any;
  myInput: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    this.title = this.navParams.get('title');
    this.friends = this.navParams.get('friends');
    this.myInput = null;
  }

  deleteFriend(friend) {
    this.areYouSureAlert(friend.name);
    // api: delete friend
    // console.log('delete ', friend);
  }

  addFriend() {
    // api: add friend
    console.log('add');
  }

  areYouSureAlert(friend) {
    let alert = this.alertCtrl.create({
      title: 'Delete ' + friend,
      message: 'Are you sure?',
      cssClass: 'alert-default',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            // api: delete current friend
          }
        }
      ]
    });
    alert.present();
  }

}
