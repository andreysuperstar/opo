import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { AddRemoteFriendsPage } from '../add-remote-friends/add-remote-friends';

@Component({
  selector: 'page-group-popover',
  templateUrl: 'group-popover.html'
})

export class GroupPopoverPage {

  title: string;
  friends: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private alertCtrl: AlertController
  ) { }

  ionViewDidLoad() {
    this.title = this.navParams.get('title');
    this.friends = this.navParams.get('friends');
  }

  addRemoteFriends() {
    this.navCtrl.push(AddRemoteFriendsPage, { title: this.title, friends: this.friends });
    this.close();
  }

  renameGroup() {
    this.renameGroupAlert(this.title);
    this.close();
  }

  deleteGroup() {
    this.areYouSureAlert();
    this.close();
  }

  close() {
    this.viewCtrl.dismiss();
  }

  renameGroupAlert(title) {
    let alert = this.alertCtrl.create({
      title: 'Enter name a new group',
      cssClass: 'alert-default',
      inputs: [
        {
          name: 'groupName',
          placeholder: 'Enter name a new group',
          value: title
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: data => {
            // api: rename current group
          }
        }
      ]
    });
    alert.present();
  }

  areYouSureAlert() {
    let alert = this.alertCtrl.create({
      title: 'Delete ' + this.title + ' group',
      message: 'Are you sure?',
      cssClass: 'alert-default',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            // api: delete current group
          }
        }
      ]
    });
    alert.present();
  }

}
