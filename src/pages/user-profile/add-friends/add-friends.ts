import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FindFriendsPage } from '../../user-profile/find-friends/find-friends';
import { DataService } from '../../../providers/data';
import { NetworkFriendsPage } from '../../user-profile/network-friends/network-friends';
import { FriendsPage } from '../../user-profile/friends/friends';

@Component({
  selector: 'page-add-friends',
  templateUrl: 'add-friends.html'
})
export class AddFriendsPage {

  friends: any[];
  networks: any[];
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public data: DataService,
    public alertCtrl: AlertController,
  ) {}

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.friends = data.friends;
      this.networks = data.networks;
    });
  }

  onSkip() {
    this.navCtrl.setRoot(FriendsPage);
  }

  showItem(item) {
    let alert = this.alertCtrl.create({
      title: 'OPO wants to open <br>' + item.name,
      cssClass: 'alert-default',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'Open',
          handler: () => {
            this.navCtrl.push(NetworkFriendsPage, {friends: item.friends, title: item.name});
          }
        }
      ]
    });
    alert.present();
  }

  findUsers() {
    this.navCtrl.push(FindFriendsPage);
  }


}
