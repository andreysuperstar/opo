import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { ProfilePopoverPage } from '../profile/tabs/profile-popover/profile-popover';
import { DataService } from '../../../providers/data';


@Component({
  selector: 'page-members-list',
  templateUrl: 'members-list.html'
})
export class MembersListPage {

  profilePicture: string;
  profileBackground: any;
  participants: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataService) {
    this.profilePicture = 'assets/img/profile-picture.jpg';
    this.profileBackground = {
      'background-image': `url(${this.profilePicture})`
    };
  }

  ionViewDidLoad() {
    // this.data.getData().subscribe(data => {
    //   this.friends = data.friends;
    // });
    this.participants = this.navParams.get('participants');
    console.log(this.participants);
  }

}
