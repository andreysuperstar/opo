import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { DataService } from '../../../providers/data';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  searchPeople: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public data: DataService) { }

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.searchPeople = data.searchPeople;
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
