import { AddFriendsPage } from '../../../add-friends/add-friends';
import { Component } from '@angular/core';
import { App, NavController } from 'ionic-angular';

import { DataService } from '../../../../../providers/data';


@Component({
  selector: 'page-requests-tab',
  templateUrl: 'requests-tab.html'
})
export class RequestsTabPage {
  requests: any[];
  myInput: string;

  constructor(public navCtrl: NavController, public data: DataService, public app: App) { }

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.requests = data.requests;
    });
    this.myInput = null;
  }

  addFriends() {
    console.log('add friends');
  }

  decline() {
    console.log('decline');
  }

  onAdd(friend, i) {
    this.requests[i].isAdded = !this.requests[i].isAdded;
  }

  goToAddFriends() {
    this.app.getRootNav().push(AddFriendsPage);
  }

}
