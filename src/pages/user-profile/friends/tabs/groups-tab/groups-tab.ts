import { Component } from '@angular/core';
import { NavController, AlertController, ToastController, App } from 'ionic-angular';
import { DataService } from '../../../../../providers/data';
import { GroupPage } from '../../../group/group';

@Component({
  selector: 'page-groups-tab',
  templateUrl: 'groups-tab.html'
})
export class GroupsTabPage {
  groups: any[];
  myInput: string;

  constructor(
    public app: App,
    public navCtrl: NavController,
    public data: DataService,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController
  ) { }

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.groups = data.groups;
    });
    this.myInput = null;
  }

  back() {
    this.navCtrl.pop(GroupsTabPage);
  }

  showGroup(group) {
    // this.app.getRootNav().setRoot(GroupPage, { group: group });
    this.app.getRootNav().push(GroupPage, { group: group });
  }


  toast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }


  newGroupAlert() {
    let alert = this.alertCtrl.create({
      title: 'Enter name a new group',
      inputs: [
        {
          name: 'groupName',
          placeholder: 'Enter name a new group'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: data => {
            if (data.groupName !== "") {
              // api: add new group;
              // api: show all groups;
              console.log(data.groupName);
            } else {
              this.toast('Please fill all fields');
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  addNewGroup() {
    this.newGroupAlert();
  }






}
