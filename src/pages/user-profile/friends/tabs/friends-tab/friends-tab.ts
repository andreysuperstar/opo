import { Component } from '@angular/core';
import { App, NavController } from 'ionic-angular';
import { DataService } from '../../../../../providers/data';
import { GroupsTabPage } from '../groups-tab/groups-tab';
import { ProfilePage } from '../../../../user-profile/profile/profile';
import { AddFriendsPage } from '../../../add-friends/add-friends';
import { RequestsTabPage } from '../requests-tab/requests-tab';

@Component({
  selector: 'page-friends-tab',
  templateUrl: 'friends-tab.html'
})
export class FriendsTabPage {
  friendsTabSearchInput = "";
  friends: any[];
  countRequests: number;
  myInput: string;

  constructor(public app: App, public navCtrl: NavController, public data: DataService) { }

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.friends = data.friends;
      this.countRequests = data.requests.length;
    });
    this.myInput = null;
  }

  onSelectAll() {
    console.log('Select All clicked');
  }

  onAdd(friend) {
    this.app.getRootNav().setRoot(ProfilePage);
  }

  onSave() {
    console.log('Checkmark clicked');
  }

  onInput(e) {
    console.log(e.target.value);
  }

  onCancel(e) {
    console.log('cancel');
  }

  createGroup() {
    console.log('create group');
  }

  goToCreateGroupPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.navCtrl.push(GroupsTabPage);
  }

  navigateToProfile() {
    this.app.getRootNav().push(ProfilePage);
  }


  goToAddFriends() {
    this.app.getRootNav().push(AddFriendsPage);
  }

  showRequests(){
    this.navCtrl.push(RequestsTabPage);
  }
}
