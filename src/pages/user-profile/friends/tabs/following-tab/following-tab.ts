import { Component } from '@angular/core';
import { App, NavController } from 'ionic-angular';
import { DataService } from '../../../../../providers/data';
import { AddFriendsPage } from '../../../add-friends/add-friends';
import { ProfilePage } from '../../../profile/profile';

@Component({
  selector: 'page-following-tab',
  templateUrl: 'following-tab.html'
})
export class FollowingTabPage {
  following: any[];
  myInput: string;

  constructor(public navCtrl: NavController, public data: DataService, public app: App) { }

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.following = data.following;
    });
    this.myInput = null;
  }

  goToAddFriends() {
    this.app.getRootNav().push(AddFriendsPage);
  }

  navigateToProfile() {
    this.app.getRootNav().push(ProfilePage);
  }

}
