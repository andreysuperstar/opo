import { Component } from '@angular/core';
import { App, NavController } from 'ionic-angular';
import { DataService } from '../../../../../providers/data';
import { AddFriendsPage } from '../../../add-friends/add-friends';
import { ProfilePage } from '../../../profile/profile';

@Component({
  selector: 'page-followers-tab',
  templateUrl: 'followers-tab.html'
})
export class FollowersTabPage {
  followers: any[];
  myInput: string;

  constructor(public navCtrl: NavController, public data: DataService, public app: App) { }

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.followers = data.followers;
    });
    this.myInput = null;
  }

  goToAddFriends() {
    this.app.getRootNav().push(AddFriendsPage);
  }

  navigateToProfile() {
    this.app.getRootNav().push(ProfilePage);
  }

}








