import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DataService } from '../../../providers/data';
import { FriendsTabPage } from './tabs/friends-tab/friends-tab';
import { FollowingTabPage } from './tabs/following-tab/following-tab';
import { FollowersTabPage } from './tabs/followers-tab/followers-tab';
import { GroupsTabPage } from './tabs/groups-tab/groups-tab';
import { SearchPage } from '../search/search';


@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html'
})
export class FriendsPage {

  myInput: string;
  countRequests: number;

  constructor(public navCtrl: NavController, public data: DataService) {
    this.myInput = '';
  }

  ionViewDidLoad() {
    this.data.getData().subscribe(data => {
      this.countRequests = data.requests.length;
    });
  }

  search() {
    this.navCtrl.push(SearchPage);
  }

  tab1 = FriendsTabPage;
  tab2 = FollowingTabPage;
  tab3 = FollowersTabPage;
  tab4 = GroupsTabPage;

}
