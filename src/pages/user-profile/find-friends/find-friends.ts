import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-find-friends',
  templateUrl: 'find-friends.html'
})
export class FindFriendsPage {

  myInput: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    this.myInput = null;
  }

}
