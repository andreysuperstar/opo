import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { AddItemPage } from '../add-item/add-item';
import { InviteFriendsPage } from '../invite-friends/invite-friends';
import { RoomPage } from '../room/room';


@Component({
  selector: 'page-create-room',
  templateUrl: 'create-room.html'
})
export class CreateRoomPage {
  constructor(
    public navCtrl: NavController
  ) { }

  goToAddItem(){
    this.navCtrl.push(AddItemPage);
  }

  invite() {
    this.navCtrl.push(InviteFriendsPage);
  }
  toRoom(){
    this.navCtrl.setRoot(RoomPage);
  }

}
