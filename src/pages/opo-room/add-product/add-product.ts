import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';


@Component({
  selector: 'page-add-product',
  templateUrl: 'add-product.html'
})
export class AddProductPage {
  // addProductModal: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController
  ) { }

  ionViewDidLoad() { }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  done() {
    console.log('done');
    this.viewCtrl.dismiss();
  }

}
