import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ShareTabPage } from './tabs/share-tab/share-tab';
import { InviteFriendsTabPage } from './tabs/invite-friends-tab/invite-friends-tab';

@Component({
  selector: 'page-invite-friends',
  templateUrl: 'invite-friends.html'
})
export class InviteFriendsPage {

  tab1 = InviteFriendsTabPage;
  tab2 = ShareTabPage;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) { }


}
