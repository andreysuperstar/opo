import { Component } from '@angular/core';
import { App, NavController, NavParams, AlertController } from 'ionic-angular';
import { DataService } from '../../../../../providers/data';
import { CreateRoomPage } from '../../../create-room/create-room';
import { NetworkFriendsPage } from '../../../../user-profile/network-friends/network-friends';
import { FindFriendsPage } from '../../../../user-profile/find-friends/find-friends';

@Component({
  selector: 'page-share-tab',
  templateUrl: 'share-tab.html'
})
export class ShareTabPage {
  myInput: string;
  friends: any;
  networks: any;
  constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public data: DataService, public alertCtrl: AlertController) {}

  ionViewDidLoad() {
    this.myInput = '';
    this.data.getData().subscribe(data => {
      this.friends = data.friends;
      this.networks = data.networks;
    });
  }

  onDone() {
    this.navCtrl.push(CreateRoomPage)
  }

  showItem(item) {
    let alert = this.alertCtrl.create({
      title: 'OPO wants to open <br>' + item.name,
      cssClass: 'alert-default',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'Open',
          handler: () => {
            this.app.getRootNav().push(NetworkFriendsPage, {friends: item.friends, title: item.name});
          }
        }
      ]
    });
    alert.present();
  }

  findUsers() {
    this.navCtrl.push(FindFriendsPage);
  }

}
