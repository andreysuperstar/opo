import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DataService } from '../../../../../providers/data';
import { CreateRoomPage } from '../../../create-room/create-room';

@Component({
  selector: 'page-invite-friends-tab',
  templateUrl: 'invite-friends-tab.html'
})
export class InviteFriendsTabPage {
  myInput: string;
  friends: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataService) {}

  ionViewDidLoad() {
    this.myInput = '';
    this.data.getData().subscribe(data => {
      this.friends = data.friends;
    });
  }

  onDone() {
    this.navCtrl.push(CreateRoomPage)
  }


}
