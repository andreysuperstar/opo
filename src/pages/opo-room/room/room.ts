import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { AddProductPage } from '../../opo-room/add-product/add-product';
import { ProductPage } from '../../user-profile/product/product';
import { ProfileMainPage } from '../../user-profile/profile/tabs/profile-main/profile-main';
import { DataService } from '../../../providers/data';
import { Storage } from '@ionic/storage';

// Socket.IO
import { ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';
import * as io from 'socket.io-client';


@Component({
  selector: 'page-room',
  templateUrl: 'room.html'
})
export class RoomPage {

  @ViewChild(Content) content: Content;
  friends: any[];
  messages: Array<any>;
  socketHost: string = 'http://opo-95765.app.xervo.io/';
  socket: any;
  username: string;
  avatar: string;
  message: string;
  base64Image: string;
  showUserMenu: boolean;

  constructor(
    public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public modalCtrl: ModalController, public data: DataService, public storage: Storage) {

    this.showUserMenu = false;

    this.messages = [];
    this.avatar = 'https://avatars.io/facebook/heather';

    this.socket = io.connect(this.socketHost);

    this.socket.on('server online', () => {
      
      let data = {
        message: `${this.username || '%username%' } online`
      };

      console.log('client online', data);
      this.socket.emit('client online', data);

    });

    this.socket.on('message', message => {
      this.messages.push(message);
      this.content.scrollToBottom();
    });

    this.socket.on('server offline', () => {
        let data = {
          system: true,
          username: this.username,
          avatar: this.avatar,
          message: `${this.username || '%username%' } offline`,
          date: Date.now()
        };

        console.log('client offline', data);
        this.socket.emit('client offline');
        // this.messages.push(data);
        // this.content.scrollToBottom();
    });

  }




  ionViewDidLoad() {
    console.log('ionViewDidLoad RoomPage');
    this.data.getData().subscribe(data => {
      this.friends = data.friends;
      // let random = Math.floor(Math.random() * 14);
      // this.username = data.friends[random].name;
      // this.avatar = data.friends[random].avatar;
      // let user = this.navParams.get('user');
      // if (user) {
      //   this.username = user.name;
      //   if (user.avater) {
      //     this.avatar = user.avatar;
      //   }
      // }
    });

        this.storage.get('user').then((data) => {
        if (data) {
          this.username = data.name;
          this.avatar = data.avatar;
        }
    });
  }

  sendMessage(message) {
    let data = {
      username: this.username,
      avatar: this.avatar,
      message: message.message
    };

    this.socket.emit('message', data);

    this.message = null;

    console.log(message);
    console.log('data', data);
  }

  addProductModal() {
    let modal = this.modalCtrl.create(AddProductPage);
    modal.present();
  }

  takePhoto() {
    let options = {
      quality: 100,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 800,
      targetHeight: 800,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    Camera.getPicture(options).then((imageData) => {
      this.base64Image = "data:image/jpeg;base64," + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  selectFromGallery() {
    let options = {
      quality: 100,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: Camera.EncodingType.JPEG,
      correctOrientation: true,
      allowEdit: true,
      targetWidth: 800,
      targetHeight: 800
    };
    Camera.getPicture(options).then((imageData) => {
      this.base64Image = "data:image/jpeg;base64," + imageData;
    }, (err) => {
      // Handle error
    });

  }

  uploadImgActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Choose the action',
      buttons: [
        {
          text: 'Gallery',
          handler: () => {
            this.selectFromGallery();
          }
        }, {
          text: 'Take photo',
          role: 'destructive',
          handler: () => {
            this.takePhoto();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  inviteUser() {
    console.log('invite');
  }

  showProductModal() {
    let modal = this.modalCtrl.create(ProductPage);
    modal.present();
  }

  star(e) {
    e.preventDefault();
    console.log('star');
  }

  delete() {
    console.log('delete');
  }

  toggleUserMenu() {
    this.showUserMenu = !this.showUserMenu;
  }

  viewProfile(user?) {
    if (user) {
      this.navCtrl.push(ProfileMainPage, { user: 'user'});
    } else {
      this.navCtrl.push(ProfileMainPage);
    }
  }

}
