import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-password',
  templateUrl: 'password.html'
})
export class PasswordPage {
  text: string = 'Forgot your password?';
  subtext:string = 'Enter your email to find your account';
  email: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PasswordPage');
  }

  onRestore() {
    console.log('Restore Password button  clicked');
  }

}
