import { Component } from '@angular/core';
import { App, NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { PasswordPage } from '../../authorization/password/password';
import { SignupPage } from '../../authorization/signup/signup';
import { MutualFriendsPage } from '../../global/mutual-friends/mutual-friends';
import { ProfilePage } from '../../user-profile/profile/profile';
import { DataService } from '../../../providers/data';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  users: any;
  text: string = 'Maecenas sed diam eget risus varius blandit sit amet non magna';
  login: { email?: string, password?: string } = {};
  submitted = false;

  constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public dataService: DataService, public menu: MenuController, public storage: Storage, public toastCtrl: ToastController) {

    // this.login = {
    //   email: "firstuser@gmail.com",
    //   password: "opo007"
    // };
  }

  ionViewDidLoad() {
    this.menu.close();
    this.menu.enable(false);
    this.menu.swipeEnable(false);

    this.dataService.getData()
      .subscribe(data => {
        this.users = data.users;
      });
  }

  // ionViewWillLeave() {
  //   this.menu.enable(true);
  //   this.menu.swipeEnable(true);
  // }


  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  checkUser(form, user) {
    if (form.value.email === user.email && form.value.password === user.password) {
      return user;
    }
  }





  onLogin(form) {
    this.submitted = true;
    if (form.valid) {
      let user = this.users.find(el => this.checkUser(form, el));
      if (user) {
        console.log(user);
        this.storage.set('user', user);

        this.storage.get('user').then((user) => {
          if (user) {
            this.navCtrl.setRoot(ProfilePage);
          }
        });

      } else {
        this.presentToast("Can't login: username/password combination is incorrect");
      }
    }
  }

  onSignup() {
    this.navCtrl.push(SignupPage);
  }

  onForgotPassword() {
    this.navCtrl.push(PasswordPage);
  }

  onFacebook() {
    this.navCtrl.push(MutualFriendsPage);
  }

  onGmail() {
    this.navCtrl.push(MutualFriendsPage);
  }

}
