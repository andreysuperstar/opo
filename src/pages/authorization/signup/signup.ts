import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { TermsPage } from '../../authorization/terms/terms';
import { ProfileSetupPage } from '../../user-profile/profile-setup/profile-setup';
import { MutualFriendsPage } from '../../global/mutual-friends/mutual-friends';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  text: string = 'Maecenas sed diam eget risus varius blandit sit amet non magna';
  login: { username?: string, fullname?: string, password?: string } = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController) { }




  ionViewDidLoad() {

  }

  ionViewWillLeave() {

  }




  onSignup() {
    
    this.navCtrl.push(ProfileSetupPage);
  }

  onTerms() {
    this.navCtrl.push(TermsPage);
  }

  onFacebook() {
    this.navCtrl.push(MutualFriendsPage);
  }

  onGmail() {
    this.navCtrl.push(MutualFriendsPage);
  }

}
