import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { FindFriendsPage } from '../../user-profile/find-friends/find-friends';
import { DataService } from '../../../providers/data';
import { NetworkFriendsPage } from '../../user-profile/network-friends/network-friends';
import { ProfilePage } from '../../user-profile/profile/profile';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-mutual-friends',
  templateUrl: 'mutual-friends.html'
})

export class MutualFriendsPage {
  users: any;
  friends: any[];
  networks: any[];
  login: { email?: string, password?: string } = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public data: DataService,
    public alertCtrl: AlertController,
    public storage: Storage,
    public menu: MenuController,
  ) {
    this.login = {
      "email": "firstuser@gmail.com",
      "password": "opo007"
    };
  }

  swipeEnable() {
    this.menu.enable(true);
    this.menu.swipeEnable(true);
  }

  ionViewDidLoad() {
    this.swipeEnable();
    this.data.getData().subscribe(data => {
      this.users = data.users;
      this.friends = data.friends;
      this.networks = data.networks;
    });
  }

  onSkip() {
    this.navCtrl.setRoot( ProfilePage);



    let user = this.users[16];

    this.storage.set('user', user);

    this.navCtrl.setRoot(ProfilePage);

  
  }

  showItem(item) {
    let alert = this.alertCtrl.create({
      title: 'OPO wants to open <br>' + item.name,
      cssClass: 'alert-default',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'Open',
          handler: () => {
            this.navCtrl.push(NetworkFriendsPage, {friends: item.friends, title: item.name});
          }
        }
      ]
    });
    alert.present();
  }

  findUsers() {
    this.navCtrl.push(FindFriendsPage);
  }


}
