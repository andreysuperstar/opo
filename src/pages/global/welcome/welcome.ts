import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { LoginPage } from '../../authorization/login/login';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {
  showSkip = true;
  slides = [
    { heading: 'What is OPO?', imageUrl: 'assets/img/welcome/2.jpg' },
    { heading: 'Why OPO important?', imageUrl: 'assets/img/welcome/3.jpg' },
    { heading: 'What you can do in OPO application', imageUrl: 'assets/img/welcome/4.jpg' },
    { heading: 'Let get started with OPO',imageUrl: 'assets/img/welcome/5.jpg', last: true }
  ];

  constructor(public navCtrl: NavController, public menu: MenuController) { }
    
  ionViewDidLoad() {
    this.menu.close();
    this.menu.enable(false);
    this.menu.swipeEnable(false);
  }

  startApp() {
    this.navCtrl.setRoot(LoginPage)
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd;
  }

}
