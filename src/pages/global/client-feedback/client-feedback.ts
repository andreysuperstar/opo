import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-client-feedback',
  templateUrl: 'client-feedback.html'
})
export class ClientFeedbackPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientFeedbackPage');
  }

}
