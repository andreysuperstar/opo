import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-app-general-settings',
  templateUrl: 'app-general-settings.html'
})
export class AppGeneralSettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppGeneralSettingsPage');
  }

}
