import { Component, Input } from '@angular/core';
import { App, NavController } from 'ionic-angular';
import { ProfileMainPage } from '../../pages/user-profile/profile/tabs/profile-main/profile-main';
import { ProfileOtherPage } from '../../pages/user-profile/profile/profile-other/profile-other';
import { RoomPage } from '../../pages/opo-room/room/room';

@Component({
  selector: 'votes-card',
  templateUrl: 'votes-card.html'
})
export class VotesCardComponent {

  @Input() friend: any;

  constructor(public app: App, public navCtrl: NavController) {
    console.log('Hello ProfileCard Component');
  }

  navigateToRoom() {
    this.app.getRootNav().push(RoomPage);
  }

  navigateToProfile() {
    this.app.getRootNav().setRoot(ProfileOtherPage);
  }

  votePhoto(friend, image) {
    if (!friend.isVoted) {
      let random = Math.floor(Math.random() * 2);
      image.heartOutline = random;
      image.heart = true;
      friend.isVoted = true;
    }
  }

}
