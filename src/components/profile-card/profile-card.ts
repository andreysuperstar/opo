import { Component, Input } from '@angular/core';
import { App, NavController } from 'ionic-angular';
import { ProfileMainPage } from '../../pages/user-profile/profile/tabs/profile-main/profile-main';
import { ProfileOtherPage } from '../../pages/user-profile/profile/profile-other/profile-other';
import { RoomPage } from '../../pages/opo-room/room/room';
import { MembersListPage } from '../../pages/user-profile/members-list/members-list';

@Component({
  selector: 'profile-card',
  templateUrl: 'profile-card.html'
})
export class ProfileCardComponent {

  @Input() friend: any;

  constructor(
    public navCtrl: NavController,
    public app: App
  ) {
    // console.log('Hello ProfileCard Component');
  }

  navigateToRoom() {
    this.app.getRootNav().push(RoomPage);
  }

  navigateToProfile(friend) {
    console.log('Component Friend', friend);
    this.app.getRootNav().setRoot(ProfileOtherPage, { 'user': friend });
  }

  goToMembersList(participants) {
    this.app.getRootNav().push(MembersListPage, { participants: participants });
  }



}
