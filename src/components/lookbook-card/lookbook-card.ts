import { Component, Input } from '@angular/core';
import { App, NavController } from 'ionic-angular';
import { ProfileMainPage } from '../../pages/user-profile/profile/tabs/profile-main/profile-main';
import { ProfileOtherPage } from '../../pages/user-profile/profile/profile-other/profile-other';
import { RoomPage } from '../../pages/opo-room/room/room';
import { ProfileLookbookItemPage } from '../../pages/user-profile/profile/tabs/profile-lookbook-item/profile-lookbook-item';

@Component({
  selector: 'lookbook-card',
  templateUrl: 'lookbook-card.html'
})
export class LookbookCardComponent {

  @Input() friend: any;

  constructor(public app: App, public navCtrl: NavController) {}

  navigateToRoom() {
    this.app.getRootNav().push(RoomPage);
  }

  navigateToProfile() {
    this.app.getRootNav().setRoot(ProfileOtherPage);
  }

  openItem() {
    this.navCtrl.push(ProfileLookbookItemPage);
  }

}
