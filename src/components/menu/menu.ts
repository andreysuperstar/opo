import { Component, Input } from '@angular/core';
import { App, Nav, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import {
  LoginPage,
  HomeFeedPage,
  ProfilePage,
  AddFriendsPage,
  FriendsPage,
  CreateRoomPage,
  AppGeneralSettingsPage
} from '../../pages';

@Component({
  selector: 'menu',
  templateUrl: 'menu.html'
})
export class MenuComponent {

  @Input() nav: Nav;
  avatar: string;
  name: string;
  email: string;
  feed: number;
  pages: Array<{ title: string, component?: any, icon: string, feed?: number }>;

  constructor(public app: App, public menuCtrl: MenuController, public storage: Storage) {
    this.pages = [
      { title: 'My Feed', component: HomeFeedPage, icon: 'ios-home-outline', feed: this.feed },
      { title: 'My Profile', component: ProfilePage, icon: 'ios-person-outline' },
      { title: 'My Friends', component: FriendsPage, icon: 'ios-people-outline' },
      { title: 'Create OPO', component: CreateRoomPage, icon: 'ios-add-outline' },
      { title: 'Settings', component: AppGeneralSettingsPage, icon: 'ios-settings-outline' },
      { title: 'Logout', component: LoginPage, icon: 'ios-exit-outline' }
    ];

    this.app.viewDidEnter
      .subscribe( view => this.firstUpdate(view.name) );
  }

  firstUpdate(viewName) {
      if(viewName === "ProfilePage") {
        this.updateUser();
      }
  }


  updateUser() {
    this.storage.get('user').then((user) => {
      if(user){
        this.avatar = user.avatar;
        this.name = user.name;
        this.email = user.email;
        this.feed = 33;
      } else {
        console.log(user);
      }
    })
  }

  openPage(page) {
    if (page.component == LoginPage) {
      this.storage.remove('user');
    }
    this.nav.setRoot(page.component);
    this.menuCtrl.close();
  }

  navigateToInviteFriends() {
    this.nav.setRoot(AddFriendsPage);
    this.menuCtrl.close();
  }

}
